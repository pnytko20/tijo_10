package pl.edu.pwsztar.domain.Service;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;


public interface ChessService {
    Boolean checkMove(FigureMoveDto figureMoveDto);
    void convert(FigureMoveDto figureMoveDto);
}
