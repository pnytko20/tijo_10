package pl.edu.pwsztar.domain.dto;

public class PointDto {

    public PointDto(int[] pointArray){
        this.x = pointArray[0];
        this.y = pointArray[1];
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    private int x;
    private int y;
}
